package view;

import controller.HighscoreManager;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Pair;
import model.GameMode;

/**
 * Classe che modella la scena per la visualizzazione dei punteggi. Estende {@link AbstractMenuView}.
 */
public class HighscoreView extends AbstractMenuView {

    private static final Font SCORE_FONT = new Font("Serif", 18);
    private static final Insets SCORE_PADDING = new Insets(10);

    /**
     * @param stage - Finestra dove verrà rappresentata la scena.
     */
    public HighscoreView(final Stage stage) {
        super(stage);
    }

    /**
     * 
     */
    @Override
    public Node centerPane() {
        final TabPane tab = new TabPane();
        tab.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

        for (final GameMode gm : GameMode.values()) {
            final Tab t = new Tab(gm.toString());
            final ScrollPane scroll = new ScrollPane();
            final GridPane grid = new GridPane();
            scroll.setContent(grid);
            scroll.setHbarPolicy(ScrollBarPolicy.NEVER);
            t.setContent(scroll);

            grid.setAlignment(Pos.CENTER);

            int i = 0;
            for (final Pair<String, Integer> pair : HighscoreManager.getInstance().getScore(gm)) {
                final Label l1 = new MyLabel(pair.getKey().toString());
                final Label l2 = new MyLabel(pair.getValue().toString());

                grid.add(l1, 0, i);
                grid.add(l2, 1, i);
                i++;
            }
            tab.getTabs().add(t);
        }

        return tab;
    }

    @Override
    public final String getTitle() {
        return "Highscore";
    }

    /**
     * Classe utilizzata per personalizzare le Label dei punteggi.
     */
    private class MyLabel extends Label {

        MyLabel(final String string) {
            super(string);

            this.setFont(SCORE_FONT);
            this.setTextFill(Color.BLUEVIOLET);
            this.setPadding(SCORE_PADDING);
            this.setPrefWidth(getStage().getWidth() / 2);
            this.setAlignment(Pos.CENTER);
        }
    }
}
