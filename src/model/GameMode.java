package model;

/**
 * Enumerazione per gestire le varie modalit� di gioco proposte.
 */
public enum GameMode {
    /**
     * Modalità classica, senza PowerUp.
     */
    CLASSIC,

    /**
     * Modalità standard, con PowerUp.
     */
    STANDARD,

    /**
     * Modalità survival. Appena una fila di mattoncini viene completamente distrutta, il gioco la rigenera.
     */
    SURVIVAL;
}
