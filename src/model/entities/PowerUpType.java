package model.entities;

import java.util.ArrayList;

import model.AdvancedGame;

/**
 * Enumerazione per gestire le tipologie di power-up ed i loro effetti.
 */
public enum PowerUpType {
    /**
     * Rende le palline di fuoco.
     */
    FIRE_BALL() {
        @Override
        public void effect(final AdvancedGame game) {
            game.getBalls().stream().forEach(b -> b.setType(BallType.FIRE_BALL));
        }

        @Override
        public void reverse(final AdvancedGame game) {
            game.getBalls().stream().forEach(b -> b.setType(BallType.STANDARD_BALL));
        }
    },

    /**
     * Triplica il numero delle palline presenti in gioco.
     */
    MULTIPLE_BALL() {
        @Override
        public void effect(final AdvancedGame game) {
            for (final Ball ball : new ArrayList<>(game.getBalls())) {
                for (final Ball daInserire : game.getFactory().multipleBall(ball)) {
                    game.addEntity(daInserire);
                }
            }
        }

        @Override
        public void reverse(final AdvancedGame game) {
            // Do nothing
        }
    },

    /**
     * Aumenta la larghezza della barra.
     */
    BIG_BAR() {

        @Override
        public void effect(final AdvancedGame game) {
            game.getBar().extendBar();
        }

        @Override
        public void reverse(final AdvancedGame game) {
            PowerUpType.LITTLE_BAR.effect(game);
        }
    },

    /**
     * Diminuisce la larghezza della barra.
     */
    LITTLE_BAR() {

        @Override
        public void effect(final AdvancedGame game) {
            game.getBar().reduceBar();
        }

        @Override
        public void reverse(final AdvancedGame game) {
            PowerUpType.BIG_BAR.effect(game);
        }
    },

    /**
     * Rende la barra in grado di sparare proiettili.
     */
    LASER_BAR() {

        private static final int MILLIS = 600;

        @Override
        public void effect(final AdvancedGame game) {
            final Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final int count = 5;
                        for (int j = 0; j < count; j++) {
                            game.getFactory().pairOfProjectile(game.getBar()).stream().forEach(i -> game.addEntity(i));
                            Thread.sleep(MILLIS);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            t.start();
        }

        @Override
        public void reverse(final AdvancedGame game) {
            //Do nothing
        }
    },

    /**
     * Incrementa il numero delle vite rimaste di 1.
     */
    INCREASE_LIVES() {

        @Override
        public void effect(final AdvancedGame game) {
            game.incLives();
        }

        @Override
        public void reverse(final AdvancedGame game) {
            //Do nothing
        }
    };

    /**
     * Metodo per applicare al gioco l'effetto del power-up.
     * @param game - {@link AdvancedGame}
     */
    public abstract void effect(AdvancedGame game);

    /**
     * Metodo per rimuovere l'effetto del powerUp (se a tempo).
     * @param game - {@link AdvancedGame}
     */
    public abstract void reverse(AdvancedGame game);
}
