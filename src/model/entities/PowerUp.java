package model.entities;

import javafx.util.Pair;
import model.AdvancedGame;
/**
 * Classe che rappresenta i Power-Up ottenibili dalla rottura dei mattoncini. Implementa {@link EntityThatMoves}
 */
//Da sistemare: il tempo del potenziamento continua a scorrere anche se il gioco e' in pausa.
public class PowerUp implements EntityThatMoves {
    private static final int POWER_UP_DURATION_IN_SEC = 15;
    private static final int SEC_TO_MILLISEC = 1000;

    private Pair<Integer, Integer> pos;
    private final int width;
    private final int height;
    private final int speedY;
    private final PowerUpType type;

    /**
     * @param width  - larghezza del power up
     * @param height - altezza del power up
     * @param pos - Pair(int, int), posizione iniziale
     * @param speed - velocit� di caduta
     * @param type - {@link PowerUpType}
     */
    public PowerUp(final int width, final int height, final Pair<Integer, Integer> pos, final int speed, final PowerUpType type) {
        this.pos = pos;
        this.width = width;
        this.height = height;
        this.speedY = speed;
        this.type = type;
    }

    @Override
    public final int getMaxX() {
        return this.pos.getKey() + this.width;
    }

    @Override
    public final int getMinX() {
        return this.pos.getKey();
    }

    @Override
    public final int getMaxY() {
        return this.pos.getValue() + this.height;
    }

    @Override
    public final int getMinY() {
        return this.pos.getValue();
    }

    @Override
    public final Pair<Integer, Integer> getPosition() {
        return this.pos;
    }

    @Override
    public final void setPosition(final int newX, final int newY) {
        this.pos = new Pair<>(newX, newY);
    }

    @Override
    public final void refreshPosition() {
        setPosition(this.pos.getKey(), this.pos.getValue() + this.speedY);
    }

    /**
     * Attiva l'effetto del power-up.
     * @param game - {@link IBasicGame} su cui applicare gli effetti
     */
    public void active(final AdvancedGame game) {
        System.out.println(this.type);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    type.effect(game);
                    Thread.sleep(POWER_UP_DURATION_IN_SEC * SEC_TO_MILLISEC);
                    type.reverse(game);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start(); 
    }

    /**
     * @return the type
     */
    public final PowerUpType getType() {
        return type;
    }
}
