package model.entities;

import javafx.util.Pair;

/**
 * Classe builder per {@link Ball}.
 */
public class BallBuilder {

    private Pair<Integer, Integer> pos;
    private int rad;
    private int sp;
    private int ballAngle;
    private BallType ballType;

    /**
     * Setta la posizione.
     * @param x - int 
     * @param y - int
     * @return this
     */
    public BallBuilder position(final int x, final int y) {
        this.pos = new Pair<>(x, y);
        return this;
    }

    /**
     * Setta il raggio della pallina.
     * @param radius - int
     * @return this
     */
    public BallBuilder radius(final int radius) {
        this.rad = radius;
        return this;
    }

    /**
     * Setta la velocit� della pallina.
     * @param speed - int
     * @return this
     */
    public BallBuilder speed(final int speed) {
        this.sp = speed;
        return this;
    }

    /**
     * Setta l'angolo di direzione.
     * 
     * @param angle - int
     * @return this
     */
    public BallBuilder angle(final int angle) {
        this.ballAngle = angle;
        return this;
    }

    /**
     * Setta la tipologia di pallina a scelta fra {@link BallType}.
     * 
     * @param type - {@link BallType}
     * @return this
     */
    public BallBuilder type(final BallType type) {
        this.ballType = type;
        return this;
    }

    /**
     * Costruisce la pallina con i parametri settati.
     * 
     * @return {@link Ball}
     */
    public Ball build() {
        return new Ball(pos.getKey(), pos.getValue(), rad, sp, ballAngle, ballType);
    }
}
